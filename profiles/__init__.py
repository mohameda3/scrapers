import yaml
from dotmap import DotMap

PROFILE = "dev"
ENVIRONMENT = DotMap(yaml.load(open('profiles/{}.yml'.format(PROFILE), 'r')))
